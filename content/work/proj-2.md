[Using Augmented Reality to Enhance Communications for the Deaf and Hard-of-Hearing Request Proposal](https://drive.google.com/open?id=0B0zZWLqRdmqEd05rZm83eWxRMEE)

Addressing a hypothetical request for proposal from the National Institute for Research (NIR),
this document propose a research on using augmented reality as a means of communication
for the deaf and hard-of-hearing. The research project is supposed to be carried out within
10 weeks of summer with a budget provided by the NIR if approved.

Although it was a fake proposal, I enjoyed writing it because I have always had this
idea as something I would like to pursue in the future. Augmented reality - and
interactive media in general - has always been a big interest of mine, so naturally
I want to find ways to apply it to more practical situations.
