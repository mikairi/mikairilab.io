[Rhetorical Analysis of Rust's Getting Started Guide](https://drive.google.com/open?id=19ht18wNNCTadTJQUZomBXJCZGaXv5h0bQ8XMb8VxT98)

In this assignment from the ENGL-361 Technical Writing class, I was tasked with analysing
the rhetorical situation of a technical document. The document I chose was the
[Rust Programming Language's Getting Started tutorial](https://doc.rust-lang.org/book/getting-started.html).
It was a document relevant to my field and also very well-written from my first impression.

My analysis goes into details on several aspects of the tutorial's creation, such as
the situation where it was created, its audience, the writing style and tone,
and the appeals that the author makes. Based on those findings, I then assess the document's
effectiveness. The assignment overall prepared me with analytical skills to idenfity
rhetorical situations and techniques essential to writing technical documents.
