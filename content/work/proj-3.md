[Extended Definition for Phishing](https://drive.google.com/open?id=1hxLo6OPenSTkgADkG1_Vs-kqYM_laLtuFS8EEwl93S8)

In this document, I provided an extended definition for phishing.
An extended definition, as the name implies, does not only define
a term as in a dictionary, but also explores various aspects
and effects related to the term.

As a web developer professionally, phishing is one of numerous
security issues directly related to my field. I wrote this extended
definition for phishing in the style of an online article, with the
intention of reaching the widest possible audience, as phishing is
a very common attack that many people everywhere can fall for.
