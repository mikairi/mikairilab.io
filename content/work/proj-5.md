[APR Scores Report & Chart](https://drive.google.com/open?id=0B0zZWLqRdmqEOUdVS0xBdUFvVmc)

This document is a short homework assignment that tests my skills in chart design
and graphics creation learned in class. The situation presented a set of academic
progress rate (APR) scores for various sport teams from a college. I was tasked with
creating charts as well as speaker's notes to show that the APR scores for the college
in question are above average.

To show scores from all sport teams and compare them to the national average in one chart,
I chose to use bar charts with different colors for the college's scores and the average
scores. The choice proved to be effective as the evaluation I received commented on
the charts being clear.
