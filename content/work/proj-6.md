[Co-op Evaluation System Design Document](https://docs.google.com/document/u/1/d/1wiHh9u0y41Q4tn7luBYBzRpxPV2kB7aMIEyOhJCw6BI/pub)

For the full final year as a Software Engineering student at RIT, I, along with three
other members, took on a project to build an evaluation system for RIT's
cooperative education (co-op) office. As part of the software development process,
we had to create a number of documentation artifacts, one of which is the Design
Document. The document provides a comprehensive view of the whole system, including
both an overview and detailed descriptions of all components.

Designed to be used within the development team first and foremost, the Design Document
is very technical. It also adopts a completely neutral tone so that information can be
delivered clearly and in a straightforward manner.
