[RIT New Student Guide](https://drive.google.com/open?id=0B0zZWLqRdmqES0lRdi1VVk9wOVU)

A guide/manual for students new to RIT, detailing the graduation requirements as well as
non-academic concerns and expectation. Designed to help new students adjust to college
lifestyle, the manual aims to present suggestions and recommendations naturally as
it explains RIT's requirements and policies.

As the document is a manual, a lot of design elements were given the focus here.
The manual is divided into small sections with bullet points instead of paragraph
where appropriate so that information is given to new students in small digestible
chunks. I also used both color and typography to clearly indicate sections as well
as small tips and suggestions.
